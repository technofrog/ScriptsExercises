import nltk
import jellyfish
import sqlite3
import sys
import logging
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

logging.basicConfig(format='%(levelname)s, %(asctime)s, %(name)s: %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

welcome = "Hi! I'm Arthur, the customer support chatbot. How can I help you?"
defaultAnswer = "The issues has been saved. We will contact you soon."

questions = (
    "The app if freezing after I click run button",
    "I don't know how to proceed with the invoice",
    "I get an error when I try to install the app",
    "It crash after I have updated it",
    "I cannot login in to the app",
    "I'm not able to download it"
            )

answers = (
        "You need to clean up the cache. Please go to ...",
        "Please go to Setting, next Subscriptions and there is the Billing section",
        "Could you plese send the log files placed in ... to ...",
        "Please restart your PC",
        "Use the forgot password button to setup a new password",
        "Probably you have an ad blocker plugin installed and it blocks the popup with the download link"
            )

similarity_treshold = 0.2

def levenshtein_distance(customer_question, question):
    normalization = max(len(customer_question), len(question))
    return 1 - jellyfish.levenshtein_distance(customer_question, question)/normalization

class sqliteDistance:
    def __init__(self):
        self.connection = sqlite3.connect('ex1.sqlite')
        self.connection.execute("CREATE VIRTUAL TABLE questions USING fts5(question, answer);")
        cursor = self.connection.cursor()
        qa = [(questions[i], answers[i]) for i in range(len(questions))]
        cursor.executemany("INSERT INTO questions(question, answer) VALUES(?, ?)", qa)
        self.connection.commit()

    def __del__(self):
        self.connection.execute("DROP TABLE questions;")
        self.connection.close()

    def __call__(self, customer_question, question):
        # TODO: this is inefficient because bm25 returns us the best answer, but the exercise constraints
        # force us to return a [0, 1] value that tells whether the question matches or not here
        cursor = self.connection.cursor()
        cursor.execute("SELECT question,bm25(questions) FROM questions WHERE question=? ORDER BY bm25(questions) DESC;", (customer_question, ))
        result = cursor.fetchall()
        if len(result) == 0:
            return 0
        else:
            if result[0][0] == question:
                return 1
            else:
                return 0.1

def get_highest_similarity(customer_question, similarity_function):
    max_similarity = 0
    highest_index = 0
    for question_id in range(len(questions)):
        similarity = similarity_function(customer_question, questions[question_id])
        logger.debug("Similarity attempt: {}".format(similarity))
        if similarity > max_similarity:
            highest_index = question_id
            max_similarity = similarity
    if max_similarity > similarity_treshold:
        return answers[highest_index]
    else:
        return "The issues has been saved. We will contact you soon."

def runChatBot(update: Update, context: CallbackContext):
    question = update.message.text
    logger.info("Asked question: {}".format(question))
    answer = "I'm afraid I don't know!"
    
    logger.debug("# Attempting Levenshtein: ")
    l_answer = get_highest_similarity(question, levenshtein_distance)
    
    logger.debug("# Attempting SQL: ")
    # TODO: this is even more inefficient. sqlInstance should be created only once,
    # but then program complains about SQLite objects used across different threads
    # Figure something out with this
    sqlInstance = sqliteDistance()
    s_answer = get_highest_similarity(question, sqlInstance)
    
    if l_answer == s_answer:
        answer = s_answer
    elif s_answer == defaultAnswer:
        answer = l_answer
    
    logger.debug("# Levehnstein answer = {}, SQL answer = {}".format(l_answer, s_answer))
    logger.info("Answer: {}".format(answer))
    update.message.reply_text(answer)
        
def main():
    with open("token.txt", "r") as tokenFile:
        token = tokenFile.readline()
        updater = Updater(token.rstrip(), use_context=True)

        dispatcher = updater.dispatcher
        dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, runChatBot))

        updater.start_polling()
        updater.idle()

if __name__ == '__main__':
    main()