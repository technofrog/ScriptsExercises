import requests

def get_intent(sentence):
    url = "http://localhost:5005/model/parse"
    payload = {'text':sentence}
    response = requests.post(url,json=payload)
    return response.json()

print(get_intent("how did the interview go"))
