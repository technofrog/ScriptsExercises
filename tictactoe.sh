#!/bin/bash

BOARD=("0" "1" "2" "3" "4" "5" "6" "7" "8")
winner=""
didAnyoneWin=1
currentPlayer="x"

function printBoard() {
	echo " ${BOARD[0]} | ${BOARD[1]} | ${BOARD[2]} "
	echo " ${BOARD[3]} | ${BOARD[4]} | ${BOARD[5]} "
	echo " ${BOARD[6]} | ${BOARD[7]} | ${BOARD[8]} "
}

function switchPlayers() {
	if [ "$currentPlayer" = "x" ]; then
		currentPlayer="o"
	else
		currentPlayer="x"
	fi
}

function checkWinner() {
	for i in 0 3 6; do
		winner="${BOARD[$i]}"
		if [ "${BOARD[$((i+1))]}" = "${winner}" ] && [ "${BOARD[$((i+2))]}" = "${winner}" ];
		then
			return 0;
		fi
	done

	for i in 0 1 2; do
		winner="${BOARD[$i]}"	
		if [ "${BOARD[$((i+3))]}" = "${winner}" ] && [ "${BOARD[$((i+6))]}" = "${winner}" ];
		then
			return 0;
		fi
	done

	winner="${BOARD[0]}"
	if [ "${BOARD[4]}" = "${winner}" ] && [ "${BOARD[8]}" = "${winner}" ]; then
		return 0;
	fi

	winner="${BOARD[2]}"
	if [ "${BOARD[4]}" = "${winner}" ] && [ "${BOARD[6]}" = "${winner}" ]; then
		return 0;
	fi

	return 1;
}

function canWeContinue() {
	for i in {0..8}; do
		if [ "${BOARD[$i]}" != "x" ] && [ "${BOARD[$i]}" != "o" ]; then
			return
		fi
	done
	didAnyoneWin=2
}

function isAllowed() {
	if [ "${BOARD[$1]}" = "x" ] || [ "${BOARD[$1]}" = "o" ];
	then
		return 1
	else
		return 0
	fi
}

while [ $didAnyoneWin -eq 1 ]; do
	printBoard
	switchPlayers
	echo "Gra $currentPlayer. Wskaz pozycje"
	read POSITION
	isAllowed $POSITION
	if [ $? -eq 1 ]; then
		echo "Niedozwolona pozycja!"
		continue
	fi

	BOARD[$POSITION]=$currentPlayer
	checkWinner
	didAnyoneWin=$?
	canWeContinue
done
if [ $didAnyoneWin -eq 0 ]; then
	echo "Wygral $winner"
else
	echo "Remis!"
fi
